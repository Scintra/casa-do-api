
const axios = require('axios');

axios.get("http://10.141.47.10:3333/users")
.then(function(res) {
    const users = res.data;

    users.forEach( function (user) {
    console.log(`Nome: ${user.nome}`);
    console.log(`Idade: ${user.idade}`);
    console.log(`Cidade: ${user.cidade}`);
    console.log(`Email: ${user.email}`);
    console.log(`RG: ${user.documentos.rg}`);
    console.log(`CPF: ${user.documentos.cpf}`);
    user.carros.forEach(function(carro, index) {
        console.log(`Carro ${index+1}: ${carro}`);
    });
});
})