

    const axios = require('axios');

    axios.get("http://senacao.tk/objetos/veiculo_array_objeto")
    .then(function(res) {
        const veiculo = res.data;
        console.log(`Marca: ${veiculo.marca}`);
        console.log(`Modelo: ${veiculo.modelo}`);
        console.log(`Ano: ${veiculo.ano}`);
        console.log(`Quilometragem: ${veiculo.quilometragem}`);
    
        veiculo.opcionais.forEach(function (opcionais , index ) {
            console.log( `Opcionais ${index+1}: ${opcionais}`) ; 
    
        });

        console.log( `Vendedor : `) ; 
        console.log(`Nome : ${veiculo.vendedor.nome}`)
        console.log( `Idade : ${veiculo.vendedor.idade}`)  ;
        console.log( `Celular : ${veiculo.vendedor.celular}`)  ;
        console.log( `Cidade : ${veiculo.vendedor.cidade}`)  ;
        
    });    
   

