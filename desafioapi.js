const axios = require('axios');

axios.get("http://senacao.tk/objetos/usuario")
.then(function(res) {

    const usuario = res.data; //data é a parte importante do objeto, o que é usado

    
    console.log(`Nome: ${usuario.nome}`);
    console.log(`Email: ${usuario.email}`);
    console.log(`Telefone: ${usuario.telefone}`);
    console.log( "-----");
   
    usuario.conhecimentos.forEach(function(conhecimentos, index) {
        console.log(`Conhecimentos ${index+1}: ${conhecimentos}`);
        console.log("------");
    });


    console.log( ` Endereço :`);
    console.log(`Rua: ${usuario.endereco.rua}`);

    console.log(`Numero: ${usuario.endereco.numero}`);

    console.log(`Bairro: ${usuario.endereco.bairro}`);

    console.log(`Cidade: ${usuario.endereco.cidade}`);

    console.log(`UF: ${usuario.endereco.uf}`);
    
    usuario.qualificacoes.forEach(function(qualificacoes) {
        console.log(`Qualificações : ${qualificacoes.nome}`);
        console.log(`Instituição : ${qualificacoes.instituicao}`);
        console.log(`Ano : ${qualificacoes.ano}`);
        console.log( "-------");
    });

});